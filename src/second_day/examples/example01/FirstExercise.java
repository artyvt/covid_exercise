package second_day.examples.example01;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

public class FirstExercise {
    public static void main(String[] args) {

        while (true) {
            Scanner scanner = new Scanner(System.in);
//            Scanner scanner2 = new Scanner(System.in);
            System.out.println("Please choose operation (+,-,/,*):");
            String operation = scanner.nextLine();

            System.out.println("Please enter first number:");
            BigDecimal firstNumber = scanner.nextBigDecimal();
            System.out.println("Please enter second number:");
            BigDecimal secondNumber = scanner.nextBigDecimal();

            BigDecimal result = null;

            if (operation.equals("+")) {
                result = sum(firstNumber, secondNumber);
            } else if (operation.equals("-")) {
                result = subtract(firstNumber, secondNumber);
            } else if (operation.equals("*")) {
                result = multiply(firstNumber, secondNumber);
            } else if (operation.equals("/")) {
                result = divide(firstNumber, secondNumber);
            }
            System.out.println("Result is: " + result);
        }
    }

    private static BigDecimal sum(BigDecimal a, BigDecimal b) {
        return a.add(b);
    }

    private static BigDecimal subtract(BigDecimal a, BigDecimal b) {
        return a.subtract(b);
    }

    private static BigDecimal multiply(BigDecimal a, BigDecimal b) {
        return a.multiply(b);
    }

    private static BigDecimal divide(BigDecimal a, BigDecimal b) {
        BigDecimal result = null;
        try {
            result = a.divide(b, 5, RoundingMode.HALF_UP);
//            throw new RuntimeException("Mystical mistake");
        } catch (ArithmeticException e) {
            System.out.println("Can't divide by zero");
        }
//        catch (RuntimeException e2) {
//            System.out.println("Mystical mistake accrued!");
//        }
        return result;
    }
}
