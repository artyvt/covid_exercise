package second_day.examples.example01.practice01;

public class MyList<E> {

    private E value;

    private MyList nextValue;

    public MyList(E value) {
        this.value = value;
    }

    public void add(E newValue) {
        if (nextValue != null) {
            nextValue.add(newValue);
        } else {
            this.nextValue = new MyList(newValue);
        }
    }

    public void print() {
        System.out.println(value);
        if (nextValue != null) {
            nextValue.print();
        }
    }
}
