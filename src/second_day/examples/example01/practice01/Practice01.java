package second_day.examples.example01.practice01;

import java.time.LocalDate;

public class Practice01 {
    public static void main(String[] args) {
        MyList list = new MyList(135);
        list.add(44);
        list.add(25);
        list.add("another crazy value");
        list.add(16);
        list.add(94);
        list.add("Amazing Value");
        list.add(LocalDate.of(2020, 8, 15));

        list.print();


        MyList<Integer> myList02 = new MyList<>(15);
        myList02.add(44);
        myList02.add(99);
        myList02.add(111);

        myList02.print();
    }
}
