package second_day.practice.practice02;

import second_day.practice.practice02.Products.Milk;
import second_day.practice.practice02.Products.Potatoes;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {
        Milk milk = new Milk(BigDecimal.valueOf(2));
        System.out.println(milk.getProductText());

        Potatoes potatoes = new Potatoes(BigDecimal.valueOf(4));
        System.out.println(potatoes.getProductText());
    }
}
