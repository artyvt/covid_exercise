package second_day.practice.practice02.AbstractClasses;

import java.math.BigDecimal;

public abstract class LiquiProduct extends Product {

    public LiquiProduct(BigDecimal amount) {
        super(amount);
    }

    @Override
    public String getMeasurementUnit() {
        return "litres";
    }
}
