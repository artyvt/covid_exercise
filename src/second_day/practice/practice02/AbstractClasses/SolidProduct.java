package second_day.practice.practice02.AbstractClasses;

import java.math.BigDecimal;

public abstract class SolidProduct extends Product {

    public SolidProduct(BigDecimal amount) {
        super(amount);
    }

    @Override
    public String getMeasurementUnit() {
        return "Kilograms";
    }
}
