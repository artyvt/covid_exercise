package second_day.practice.practice02.AbstractClasses;

import java.math.BigDecimal;

public abstract class Product {
    private BigDecimal amount;

    public Product(BigDecimal amount) {
        this.amount = amount;
    }

    public abstract BigDecimal getPricePerUnit();

    public BigDecimal getTotalPrice() {
        BigDecimal temp = this.amount.multiply(getPricePerUnit());
        return temp;
    }

    public abstract String getProductName();

    public abstract String getMeasurementUnit();


    public String getProductText() {
        return getProductName() + " " + amount + " " + getMeasurementUnit() + " " + getTotalPrice();
    }
}
