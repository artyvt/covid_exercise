package second_day.practice.practice02.Products;

import second_day.practice.practice02.AbstractClasses.LiquiProduct;

import java.math.BigDecimal;

public class Milk extends LiquiProduct {
    private BigDecimal pricePerUnit = BigDecimal.valueOf(2);

    public Milk(BigDecimal amount) {
        super(amount);
    }

    @Override
    public BigDecimal getPricePerUnit() {
        return pricePerUnit;
    }

    @Override
    public String getProductName() {
        return "Milk";
    }


}
