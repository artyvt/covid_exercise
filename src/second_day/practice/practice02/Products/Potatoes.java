package second_day.practice.practice02.Products;

import second_day.practice.practice02.AbstractClasses.SolidProduct;

import java.math.BigDecimal;

public class Potatoes extends SolidProduct {
    private BigDecimal pricePerUnit = BigDecimal.valueOf(4);

    @Override
    public BigDecimal getPricePerUnit() {
        return pricePerUnit;
    }

    @Override
    public String getProductName() {
        return "Potatoes";
    }

    public Potatoes(BigDecimal amount) {
        super(amount);
    }
}
