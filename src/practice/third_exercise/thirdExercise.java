package practice.third_exercise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class thirdExercise {

    public static void main(String[] args) throws IOException {
        while (true) {
            System.out.println("Please enter IP:");
            Scanner scanner = new Scanner(System.in);
            String ipAddress = scanner.nextLine();
            String result = doSomethingInteresting(ipAddress);
            printCountryandCity(result);
            System.out.println();
        }
    }

    private static String doSomethingInteresting(String ipAddress) throws IOException {
        URL url = new URL("http://ip-api.com/csv/" + ipAddress);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        InputStreamReader inputStreamReader = new InputStreamReader(conn.getInputStream());
        BufferedReader br = new BufferedReader(inputStreamReader);
        String output = br.readLine();
        conn.disconnect();
        return output;
    }

    private static void printCountryandCity(String fromApi) {
        String[] strings = fromApi.split(",");
        System.out.println("Country: " + strings[1] + " City: " + strings[4]);
    }
}
