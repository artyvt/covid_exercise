package practice.read_from_file;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        File sampleFile = new File("/home/artursvaitilavics/IdeaProjects/JavaRiga10_v003/src/practice/read_from_file/sample.txt");
        Scanner scanner = new Scanner(sampleFile);
        List<Person> personList = new ArrayList<>();

        String fileInfo = "";

        while (scanner.hasNextLine()) {
            fileInfo = scanner.nextLine();
            String[] personData = fileInfo.split(" ");
            personList.add(new Person(personData[0], personData[1], personData[2]));
        }


        for (Person person :
                personList) {
            System.out.println(person.toString());
        }

    }
}
