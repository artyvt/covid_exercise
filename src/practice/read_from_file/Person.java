package practice.read_from_file;

public class Person {
    String name;
    String surName;
    Integer phoneNumber;

    public Person(String name, String surName, String phoneNumber) {
        this.name = name;
        this.surName = surName;
        this.phoneNumber = Integer.parseInt(phoneNumber);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                ", phoneNumber=" + phoneNumber +
                '}';
    }
}
