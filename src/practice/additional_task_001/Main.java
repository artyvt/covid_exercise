package practice.additional_task_001;


import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Please write country name, to see daily Covid cases: ");
            String country = scanner.nextLine();
            List<Day> dayList = getDays(country.toLowerCase());
            CountryCase countryCase01 = new CountryCase(country, dayList);
            countryCase01.printCasesPerDayInCountry();
            System.out.println();
        }

    }

    private static List<Day> getDays(String country) throws IOException {
        String dayCount = doSomethingInteresting3(country);
        List<String> daysFullInfo = new ArrayList<>();
        List<Day> dayList = new ArrayList<Day>();

        dayCount = dayCount.substring(1, dayCount.length() - 2);
        String[] daysBeforeCleanup = dayCount.split("},");

        splitDays(daysBeforeCleanup, daysFullInfo);
        getDayListPerCountry(daysFullInfo, dayList);
        return dayList;
    }

    private static void getDayListPerCountry(List<String> daysFullInfo, List<Day> dayList) {
        for (String string :
                daysFullInfo) {
            String[] strings = string.split(",");
            for (int i = 0; i <= strings.length - 1; i++) {
                dayList.add(new Day(strings[strings.length - 1], strings[7]));
            }
        }
    }

    private static void splitDays(String[] daysBeforeCleanup, List<String> daysFullInfo) {
        for (String string :
                daysBeforeCleanup) {
            string = string.substring(1);
            String[] name = string.split(",");
            daysFullInfo.add(string);
        }
    }

    private static String doSomethingInteresting3(String country) throws IOException, IOException {
        String myUrl = "https://api.covid19api.com/dayone/country/" + country + "/status/confirmed/live";
        URL url = new URL(myUrl);
        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        InputStreamReader inputStreamReader = new InputStreamReader(conn.getInputStream());
        BufferedReader br = new BufferedReader(inputStreamReader);
        String output = br.readLine();
        conn.disconnect();
        return output;
    }
}
