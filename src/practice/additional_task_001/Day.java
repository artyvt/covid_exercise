package practice.additional_task_001;

public class Day {
    String date;
    String cases;

    public Day(String date, String cases) {
        this.date = date;
        this.cases = cases;
    }

    @Override
    public String toString() {
        return date + ' ' +
                cases;
    }
}
