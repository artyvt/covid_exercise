package practice.additional_task_001;

import java.util.ArrayList;
import java.util.List;

public class CountryCase {
    String name;
    List<Day> days = new ArrayList<Day>();

    public CountryCase(String name, List<Day> days) {
        this.name = name;
        this.days = days;
    }

    public void addDays(Day day) {
        days.add(day);
    }

    public void printCasesPerDayInCountry() {
        System.out.println(name + ": ");
        for (Day day :
                this.days) {
            System.out.println("\t " + day);
        }
    }
}
