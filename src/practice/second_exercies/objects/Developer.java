package practice.second_exercies.objects;

public class Developer extends Person {
    public Developer() {
        super();
        printDeveloper();
    }

    public void printDeveloper() {
        System.out.println("Developer class");
    }
}
