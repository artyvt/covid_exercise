package practice.second_exercies.objects;

public class JavaDeveloper extends Developer {

    public JavaDeveloper() {
        super();
        printJavaDeveloper();
    }

    public void printJavaDeveloper() {
        System.out.println("JavaDeveloper class");
    }

}
