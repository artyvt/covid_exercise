package examples.Exercise04;

import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
    public static void main(String[] args) {
        System.out.println("Game -- Rock Paper Scissors");
        System.out.println("Eneter your choice (rock, paper or scissors): ");
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        while (true) {


            String rawValue = scanner.nextLine();

            Rps choosenValue = null;
            try {
                choosenValue = Rps.valueOf(rawValue.toUpperCase());
            }catch (IllegalArgumentException e){
                System.out.println("you have entered wrong value");
            }

            Integer computerValue = random.nextInt(3);

            Rps whatComputerChoose = Rps.values()[computerValue];

            System.out.println("Person Chose: " + choosenValue);
            System.out.println("Computer chose: " + whatComputerChoose);

            boolean personWon = false;
            boolean isDraw = false;

            if (choosenValue == Rps.ROCK && whatComputerChoose == Rps.SCISSORS) {
                personWon = true;
            } else if (choosenValue == Rps.PAPER && whatComputerChoose == Rps.ROCK) {
                personWon = true;
            } else if (choosenValue == Rps.SCISSORS && whatComputerChoose == Rps.PAPER) {
                personWon = true;
            } else if (choosenValue == whatComputerChoose) {
                isDraw = true;
            } else {
                personWon = false;
            }

            if (personWon) {
                System.out.println("Person Won");
            } else if (isDraw) {
                System.out.println("Draw");
            } else {
                System.out.println("Computer won");
            }
            System.out.println();
        }
    }
}
