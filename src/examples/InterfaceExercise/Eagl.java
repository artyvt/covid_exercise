package examples.InterfaceExercise;

public class Eagl implements Bird , Animal{
    private int featherCount = 1000;

    @Override
    public void fly() {
        System.out.println("Eagle is flying");
    }

    @Override
    public int getFeatherCount() {
        return featherCount;
    }

    @Override
    public void walk() {
        System.out.println("Eagle is walking");
    }

    @Override
    public void dropFeather() {
        featherCount--;
    }

    @Override
    public void eat() {
        System.out.println("Eagl is eating");
    }
}
