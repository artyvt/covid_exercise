package examples.InterfaceExercise;

public abstract class AbstractAnimal {

    public void sleep() {
        System.out.println("Animal is sleeping");
    }

    public abstract void breathe();

}
