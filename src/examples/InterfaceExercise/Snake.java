package examples.InterfaceExercise;

public class Snake extends AbstractAnimal implements Animal {
    @Override
    public void eat() {
        System.out.println("snake eat freaky");
    }

    @Override
    public void breathe() {
        System.out.println("Snake breather through lungs");
    }
}
