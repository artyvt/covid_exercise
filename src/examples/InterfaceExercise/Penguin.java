package examples.InterfaceExercise;

public class Penguin implements Bird, Animal {
    int featherCount = 100;

    @Override
    public void fly() {
        System.out.println("Penguin can't fly");
    }

    @Override
    public void walk() {
        System.out.println("penguin walks funny");
    }

    @Override
    public void dropFeather() {
        featherCount--;
    }

    @Override
    public int getFeatherCount() {
        return featherCount;
    }

    @Override
    public void eat() {
        System.out.println("Penguin is eating");
    }
}
