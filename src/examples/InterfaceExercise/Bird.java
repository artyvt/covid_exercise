package examples.InterfaceExercise;

public interface Bird {

    void fly();

    void walk();

    void dropFeather();

    int getFeatherCount();
}
