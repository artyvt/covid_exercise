package examples.InterfaceExercise;

public class Main {
    public static void main(String[] args) {
        Eagl eagl = new Eagl();
        testThatBirdCanFly(eagl);
        testThatAnimalCanEat(eagl);

        Penguin penguin = new Penguin();
        testThatBirdCanFly(penguin);
        testThatAnimalCanEat(penguin);

        Snake snake = new Snake();
        testThatAnimalCanEat(snake);
        snake.sleep();
        snake.breathe();
        testThatAnimalCanSleepAndBreathe(snake);
    }

    public static void testThatBirdCanFly(Bird bird) {
        bird.fly();
    }

    public static void testThatAnimalCanEat(Animal animal) {
        animal.eat();
    }

    public static void testThatAnimalCanSleepAndBreathe(AbstractAnimal animal) {
        animal.sleep();
        animal.breathe();
    }
}
